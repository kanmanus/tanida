<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
  'as' => 'home',
  function () {
    return view('frontend.index');
  }
]);

Route::get('about-tanida', [
  'as' => 'about-tanida',
  function(){
    return view('frontend.about-tanida');
  }
]);

Route::get('room-types', [
  'as' => 'room-types',
  function(){
    return view('frontend.room-types');
  }
]);

Route::get('room-detail', [
  'as' => 'room-detail',
  function(){
    return view('frontend.room-detail');
  }
]);

Route::group(['prefix' => 'booking'], function () {
    Route::any('book-a-room', [
      'as' => 'book-a-room',
      function(){
        return view('frontend.booking.book-a-room');
      }
    ]);

    Route::any('guest-review', [
      'as' => 'guest-review',
      function(){
        return view('frontend.booking.guest-review');
      }
    ]);

    Route::any('confirmation', [
      'as' => 'booking-confirmation',
      function(){
        return view('frontend.booking.confirmation');
      }
    ]);
});

Route::get('our-facilities', [
  'as' => 'our-facilities',
  function(){
    return view('frontend.our-facilities');
  }
]);

Route::get('contact-us', [
  'as' => 'contact-us',
  function(){
    return view('frontend.contact-us');
  }
]);

/* --------------------- Admin ------------------- */
Route::group(['prefix' => '_admin'], function () {
    Route::controller('login', 'Backend\LoginController');
    Route::get('logout', 'Backend\LoginController@logout');
});
Route::group(['middleware'=>'admin','prefix' => '_admin'], function () {
    Route::get('/', function () { return view('backend.index'); });
    Route::resource('user-management','Backend\AdminController');
    Route::resource('role','Backend\AdminRoleController');
    Route::resource('page','Backend\AdminPageController');
    Route::post('check-username','Backend\CheckUsernameController@checkuser');
});

/* --------------------- Theme ------------------- */
Route::group(['prefix' => '_theme'], function () {
    Route::get('blank',function(){
        return view('backend.theme_component.blank');
    });
    Route::get('form',function(){
        return view('backend.theme_component.form');
    });
    Route::get('list',function(){
        return view('backend.theme_component.list');
    });
});
