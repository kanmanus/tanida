@extends('frontend.layout.main-layout')

@section('title', ' - Room Types')

@section('css')
  {!! Html::style('css/frontend/room-types.css') !!}
@endsection

@section('content')
  <div id="banner">
    <div id="promo-text" class="fadeVisible">ROOM TYPES</div>
  </div>

  <div id="room-types">
    <div class="container">
      <div class="room-list col-lg-10 col-lg-offset-1">
        <div class="room-item col-sm-6 col-lg-12 fadeVisible">
          <div class="room-image col-lg-push-6" style="background-image: url('{{ URL::asset('images/room-penthouse.png') }}');"></div>

          <div class="room-info">
            <div class="room-name">PENTHOUSE</div>
            <div class="hr">
              <img src="{{ URL::asset('images/hr3.png') }}">
            </div>

            <div class="room-detail">
              Get closer to that magical garden island view with a Deluxe room.
              Each has a sleek furnished terrace directly overlooking the red-tiled Old Town and Lokrum.
            </div>

            <div class="room-price">
              55,000 THB / month
            </div>

            <a href="{{ URL::route('room-detail') }}"><button class="see-more">ROOM DETAIL</button></a>
          </div>
        </div>

        <div class="room-item col-sm-6 col-lg-12 fadeVisible">
          <div class="room-image" style="background-image: url('{{ URL::asset('images/room-2-bedroom.png') }}');"></div>

          <div class="room-info">
            <div class="room-name">2 BEDROOMS</div>
            <div class="hr">
              <img src="{{ URL::asset('images/hr3.png') }}">
            </div>

            <div class="room-detail">
              Get closer to that magical garden island view with a Deluxe room.
              Each has a sleek furnished terrace directly overlooking the red-tiled Old Town and Lokrum.
            </div>

            <div class="room-price">
              55,000 THB / month
            </div>

            <a href="{{ URL::route('room-detail') }}"><button class="see-more">ROOM DETAIL</button></a>
          </div>
        </div>

        <div class="room-item col-sm-6 col-lg-12 fadeVisible">
          <div class="room-image" style="background-image: url('{{ URL::asset('images/room-1-bedroom.png') }}');"></div>

          <div class="room-info">
            <div class="room-name">1 BEDROOM</div>
            <div class="hr">
              <img src="{{ URL::asset('images/hr3.png') }}">
            </div>

            <div class="room-detail">
              Get closer to that magical garden island view with a Deluxe room.
              Each has a sleek furnished terrace directly overlooking the red-tiled Old Town and Lokrum.
            </div>

            <div class="room-price">
              55,000 THB / month
            </div>

            <a href="{{ URL::route('room-detail') }}"><button class="see-more">ROOM DETAIL</button></a>
          </div>
        </div>

        <div class="room-item col-sm-6 col-lg-12 fadeVisible">
          <div class="room-image" style="background-image: url('{{ URL::asset('images/room-studio.png') }}');"></div>

          <div class="room-info">
            <div class="room-name">STUDIO</div>
            <div class="hr">
              <img src="{{ URL::asset('images/hr3.png') }}">
            </div>

            <div class="room-detail">
              Get closer to that magical garden island view with a Deluxe room.
              Each has a sleek furnished terrace directly overlooking the red-tiled Old Town and Lokrum.
            </div>

            <div class="room-price">
              55,000 THB / month
            </div>

            <a href="{{ URL::route('room-detail') }}"><button class="see-more">ROOM DETAIL</button></a>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script>
$(function(){
  $('#banner').parallax({imageSrc: '{{ URL::asset("images/room-types-banner.png") }}'});
});
</script>
@endsection
