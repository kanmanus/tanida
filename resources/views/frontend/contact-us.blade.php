@extends('frontend.layout.main-layout')

@section('title', ' - Contact Us')

@section('css')
  {!! Html::style('css/frontend/contact-us.css') !!}
@endsection

@section('content')
  <div id="banner">
    <div id="promo-text" class="fadeVisible">CONTACT US</div>
  </div>

  <div id="contact-us">
    <div class="container">
      <div id="map-div"></div>

      <div class="main-content row">
        <div class="contact-form col-sm-6">
          <form class="box">
            <div class="row fadeVisible">
              <?php $input_name = 'firstname'; $label = 'First Name'; ?>
              <div class="form-{{ $input_name }} col-xs-6">
                <label for="{{ $input_name }}">{{ $label }}</label>
                <input type="text" id="{{ $input_name }}" name="{{ $input_name }}" placeholder="{{ $label }}">
              </div>

              <?php $input_name = 'lastname'; $label = 'Last Name'; ?>
              <div class="form-{{ $input_name }} col-xs-6">
                <label for="{{ $input_name }}">{{ $label }}</label>
                <input type="text" id="{{ $input_name }}" name="{{ $input_name }}" placeholder="{{ $label }}">
              </div>
            </div>

            <div class="row fadeVisible">
              <?php $input_name = 'email'; $label = 'E-mail'; ?>
              <div class="form-{{ $input_name }} col-xs-6">
                <label for="{{ $input_name }}">{{ $label }}</label>
                <input type="text" id="{{ $input_name }}" name="{{ $input_name }}" placeholder="{{ $label }}">
              </div>

              <?php $input_name = 'tel'; $label = 'Telephone'; ?>
              <div class="form-{{ $input_name }} col-xs-6">
                <label for="{{ $input_name }}">{{ $label }}</label>
                <input type="text" id="{{ $input_name }}" name="{{ $input_name }}" placeholder="{{ $label }}">
              </div>
            </div>

            <div class="row fadeVisible">
              <div class="form-msg">
                <label for="msg">Message</label>
                <textarea name="msg" id="msg" rows="6"></textarea>
              </div>
            </div>

            <div class="row text-center fadeVisible">
              <button type="submit" class="submit-btn">SUBMIT</button>
            </div>
          </form>
        </div>

        <div class="contact-info col-sm-6">
          <div class="headline fadeVisible">TANIDA RESIDENCE</div>

          <div class="hr fadeVisible">
            <img src="{{ URL::asset('images/hr2.png') }}">
          </div>

          <div class="content col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 fadeVisible">
            <div class="address">48 Surasak Rd., Silom, Bangrak, Bangkok 10500</div>
            <div class="tel">T. +662 630 2020-1</div>
            <div class="mobile">M. +66 89 06 55553</div>
            <div class="fax">F. +662 630 2022</div>
            <div class="website">
              <a href="http://www.tanidaresidence.com">www.anidaresidence.com</a>
            </div>
            <div class="email">
              Email: <a href="mailto: admin@tanidaresidence.com">admin@tanidaresidence.com</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script>
$(function(){
  $('#banner').parallax({imageSrc: '{{ URL::asset("images/room-types-banner.png") }}'});
});

function initMap(){
  map = new google.maps.Map(document.getElementById('map-div'), {
    center: {lat: 13.721097, lng: 100.519304},
    zoom: 17,
    scrollwheel: false
  });

  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(13.721097, 100.519304),
    map: map,
    title: 'Tanida Residence'
  });
}
</script>
<script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPD-ewN-T_CPtjF4OSRQ6CM1K1zb9mROM&callback=initMap">
</script>
@endsection
