<header>
  <div class="container">
    <div id="top-bar">
      <div class="contact-tel pull-left">+662 630 2020-1</div>

      <ul id="lang-bar" class="pull-right">
        <li><a href="#" class="active">EN</a></li>
        <li><a href="#">CH</a></li>
        <li><a href="#">JP</a></li>
      </ul>
    </div>

    <nav>
      <div class="nav-left">
        <div class="nav-left-md">
          <a href="{{ URL::route('about-tanida') }}" class="menu-item">ABOUT TANIDA</a>
          <a href="{{ URL::route('room-types') }}" class="menu-item">ROOM TYPES</a>
          <a href="{{ URL::route('our-facilities') }}" class="menu-item">OUR FACILITIES</a>
        </div>

        <div class="nav-toggle">
          <div class="nav-toggle-icon">
            <i class="glyphicon glyphicon-menu-hamburger"></i>
          </div>
        </div>
      </div>

      <a href="{{ URL::route('home') }}" id="logo" class="nav-center">
        <img src="{{ URL::asset('images/logo.png') }}" alt="Tanida Residence">
      </a>

      <div class="nav-right">
        <div class="nav-right-md">
          <div class="menu-item">GALLERY</div>
          <div class="menu-item">SPECIAL OFFERS</div>
          <a href="{{ URL::route('contact-us') }}" class="menu-item">CONTACT US</a>
        </div>

        <ul class="lang-bar pull-right">
          <li class="active">EN</li>
          <li>CH</li>
          <li>JP</li>
        </ul>
      </div>
    </nav>

    <div class="nav-xs">
      <div class="menu-item">About TANIDA</div>
      <div class="menu-item">Rooms and Facilities</div>
      <div class="menu-item">Rate</div>
      <div class="menu-item">Gallery</div>
      <div class="menu-item">Special Offers</div>
      <div class="menu-item">Contact Us</div>
    </div>
  </div>
</header>
