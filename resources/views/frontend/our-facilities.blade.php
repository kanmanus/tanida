@extends('frontend.layout.main-layout')

@section('title', ' - Our Facilities')

@section('css')
  {!! Html::style('assets/global/css/jquery.bxslider.css') !!}
  {!! Html::style('css/frontend/our-facilities.css') !!}
@endsection

@section('content')
  <div id="banner">
    <div id="promo-text" class="fadeVisible">FACILITIES & SERVICES</div>
  </div>

  <div id="our-facilities">
    <div class="container">
      <div class="headline fadeVisible">FACILITIES & SERVICES</div>

      <div class="hr fadeVisible">
        <img src="{{ URL::asset('images/hr4.png') }}">
      </div>

      <div class="content col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 fadeVisible">
        <div class="row">
          <div class="col-sm-6"><i class="glyphicon glyphicon-grain"></i> 24-hour security guards & CCTV</div>
          <div class="col-sm-6"><i class="glyphicon glyphicon-grain"></i> Concierge</div>
        </div>
        <div class="row">
          <div class="col-sm-6"><i class="glyphicon glyphicon-grain"></i> Business center with WIFI internet</div>
          <div class="col-sm-6"><i class="glyphicon glyphicon-grain"></i> Fitness Center with premium equipment</div>
        </div>
        <div class="row">
          <div class="col-sm-6"><i class="glyphicon glyphicon-grain"></i> Outdoor swimming pool</div>
          <div class="col-sm-6"><i class="glyphicon glyphicon-grain"></i> Laundry & dry cleaning service</div>
        </div>
        <div class="row">
          <div class="col-sm-6"><i class="glyphicon glyphicon-grain"></i> Sauna</div>
          <div class="col-sm-6"><i class="glyphicon glyphicon-grain"></i> Shuttle service</div>
        </div>
        <div class="row">
          <div class="col-sm-6"><i class="glyphicon glyphicon-grain"></i> Underground parking</div>
        </div>
      </div>
    </div>
  </div>

  <div id="gallery">
    <ul class="container bxslider">
      <li><img src="{{ URL::asset('images/our-facilities-01.png') }}" /></li>
      <li><img src="{{ URL::asset('images/our-facilities-02.png') }}" /></li>
      <li><img src="{{ URL::asset('images/our-facilities-03.png') }}" /></li>
      <li><img src="{{ URL::asset('images/room-penthouse.png') }}" /></li>
      <li><img src="{{ URL::asset('images/room-2-bedroom.png') }}" /></li>
      <li><img src="{{ URL::asset('images/room-studio.png') }}" /></li>
    </ul>
  </div>
@endsection

@section('script')
{!! Html::script('assets/global/scripts/jquery.bxslider.min.js') !!}
<script>
$(function(){
  $('#banner').parallax({imageSrc: '{{ URL::asset("images/our-facilities-banner.png") }}'});

  $('.bxslider').bxSlider({
    minSlides: 3,
    maxSlides: 3,
    slideWidth: 360,
    slideMargin: 10,
    controls: false
  });
});
</script>
@endsection
