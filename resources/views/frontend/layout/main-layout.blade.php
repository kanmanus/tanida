<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>{{ Config::get('constants.APP_NAME') }}@yield('title')</title>

    <!-- Responsive Window -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Custom fonts -->
    {!! Html::style('https://fonts.googleapis.com/css?family=Roboto:400,300,700') !!}

    <!-- CSS Files -->
    {!! Html::style('assets/global/css/animate.css') !!}
    {!! Html::style('assets/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('css/frontend/main.css') !!}
    @yield('css')
  </head>

  <body>
    @include('frontend.include.header')

    <div id="ui-main">
      @yield('content')
    </div>

    @include('frontend.include.footer')

    {!! Html::script('assets/global/scripts/jquery-2.1.4.js') !!}
    {!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}
    {!! Html::script('assets/global/scripts/jquery.viewportchecker.min.js') !!}
    {!! Html::script('assets/global/scripts/parallax.min.js') !!}
    {!! Html::script('js/frontend/global.js') !!}
    @yield('script')
  </body>
</html>
