@extends('frontend.layout.main-layout')

@section('title', ' - About TANIDA')

@section('css')
  {!! Html::style('css/frontend/about-tanida.css') !!}
@endsection

@section('content')
  <div id="banner">
    <div id="promo-text" class="fadeVisible">ABOUT TANIDA</div>
  </div>

  <div id="about-tanida">
    <div class="container">
      <div class="headline fadeVisible">TANIDA RESIDENCE</div>

      <div class="hr fadeVisible">
        <img src="{{ URL::asset('images/hr2.png') }}">
      </div>

      <div class="content col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 fadeVisible">
        <p>
          A stylish and sophisticated serviced residence, yet filled with the warmth of our staff’s welcome.
          Tanida is both the ideal destination for corporate executives and a true holiday hideaway in the middle of the city.
        </p>

        <p>
          The 48 apartment units are located in two 8-storey buildings; each spacious unit features a private balcony
          and warm contemporary interiors.
        </p>

        <p>
          With state-of-the-art security system, Tanida offers privacy for your serene stay. The careful attention
          for your assurance allows guests to individually control visitor access from their room; the system also
          includes AIPHONE touch screen video door phone and touch screen digital door lock in addition to CCTV and access control.
        </p>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script>
$(function(){
  $('#banner').parallax({imageSrc: '{{ URL::asset("images/about-tanida-banner.png") }}'});
});
</script>
@endsection
