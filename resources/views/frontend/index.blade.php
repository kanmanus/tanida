@extends('frontend.layout.main-layout')

@section('title', ' - Home')

@section('css')
  {!! Html::style('css/frontend/home.css') !!}
@endsection

@section('content')
  <div id="banner">
    <div id="promo-text" class="fadeVisible">
      MODERN SECURE LIVING
    </div>

    <form id="hero-content" class="fadeVisible">
      <input type="text" name="check_in" placeholder="CHECK IN">
      <input type="text" name="month" placeholder="MONTH">
      <select name="room_type">
        <option value="" selected disabled>ROOM TYPE</option>
        <option value="1">Penthouse</option>
        <option value="2">2 Bedrooms</option>
        <option value="3">1 Bedroom</option>
        <option value="4">Studio</option>
      </select>
      <input type="text" name="promotion_code" placeholder="PROMOTION CODE">
      <button type="submit">BOOK NOW</button>
    </form>
  </div>

  <div id="about-tanida">
    <div class="container fadeVisible">
      <div class="headline">
        <div class="--main">TANIDA</div>
        <div class="--sub">RESIDENCE</div>
        <img src="{{ URL::asset('images/hr.png') }}" class="hr">
      </div>

      <div class="content col-sm-8 col-sm-offset-2">
        Over 70 successful years, Teo Hong Group has evolved into Thailand's sophisticated selling electrical materials.
        Tanida Residence is one of projects in Teo Hong Group.
      </div>
    </div>
  </div>

  <div id="special-offers">
    <div class="content fadeVisible">
      <div class="--sub">Long Stay</div>
      <div class="--main">GET FREE</div>
      <div class="--sub">Personal Limousine Service</div>

      <button>BOOK NOW</button>
    </div>
  </div>

  <div id="room-types">
    <div class="container">
      <div class="headline fadeVisible">
        <span class="bold">ROOM</span> TYPES
      </div>

      <div class="hr">
        <img src="{{ URL::asset('images/hr2.png') }}">
      </div>

      <div class="room-list">
        <div class="room-item col-sm-6 fadeVisible">
          <div class="box">
            <div class="room-detail">
              <div class="room-name">PENTHOUSE</div>
              <div class="room-area">AREA: 150 m<sup>2</sup> - 170 m<sup>2</sup></div>
            </div>

            <div class="hr">
              <img src="{{ URL::asset('images/hr3.png') }}">
            </div>

            <div class="room-picture">
              <img src="{{ URL::asset('images/room-penthouse.png') }}" alt="Penthouse">

              <div class="room-extra-detail">
                <div class="room-name">PENTHOUSE</div>

                <div class="room-more-detail">
                  4 Pax Size 80-87 sq.m.<br/>
                  60,000 THB / month<br/>
                  55,000 THB / year<br/>
                </div>

                <a href="{{ URL::route('room-detail') }}"><button class="see-more">SEE MORE</button></a>
              </div>
            </div>
          </div>
        </div>

        <div class="room-item col-sm-6 fadeVisible">
          <div class="box">
            <div class="room-detail">
              <div class="room-name">2 BEDROOMS</div>
              <div class="room-area">AREA: 80 m<sup>2</sup> - 87 m<sup>2</sup></div>
            </div>

            <div class="hr">
              <img src="{{ URL::asset('images/hr3.png') }}">
            </div>

            <div class="room-picture">
              <img src="{{ URL::asset('images/room-2-bedroom.png') }}" alt="Penthouse">

              <div class="room-extra-detail">
                <div class="room-name">2 BEDROOMS</div>

                <div class="room-more-detail">
                  4 Pax Size 80-87 sq.m.<br/>
                  60,000 THB / month<br/>
                  55,000 THB / year<br/>
                </div>

                <a href="{{ URL::route('room-detail') }}"><button class="see-more">SEE MORE</button></a>
              </div>
            </div>
          </div>
        </div>

        <div class="room-item col-sm-6 fadeVisible">
          <div class="box">
            <div class="room-detail">
              <div class="room-name">1 BEDROOM</div>
              <div class="room-area">AREA: 53 m<sup>2</sup> - 75 m<sup>2</sup></div>
            </div>

            <div class="hr">
              <img src="{{ URL::asset('images/hr3.png') }}">
            </div>

            <div class="room-picture">
              <img src="{{ URL::asset('images/room-1-bedroom.png') }}" alt="Penthouse">

              <div class="room-extra-detail">
                <div class="room-name">1 BEDROOM</div>

                <div class="room-more-detail">
                  4 Pax Size 80-87 sq.m.<br/>
                  60,000 THB / month<br/>
                  55,000 THB / year<br/>
                </div>

                <a href="{{ URL::route('room-detail') }}"><button class="see-more">SEE MORE</button></a>
              </div>
            </div>
          </div>
        </div>

        <div class="room-item col-sm-6 fadeVisible">
          <div class="box">
            <div class="room-detail">
              <div class="room-name">STUDIO</div>
              <div class="room-area">AREA: 31 m<sup>2</sup></div>
            </div>

            <div class="hr">
              <img src="{{ URL::asset('images/hr3.png') }}">
            </div>

            <div class="room-picture">
              <img src="{{ URL::asset('images/room-studio.png') }}" alt="Penthouse">

              <div class="room-extra-detail">
                <div class="room-name">STUDIO</div>

                <div class="room-more-detail">
                  4 Pax Size 80-87 sq.m.<br/>
                  60,000 THB / month<br/>
                  55,000 THB / year<br/>
                </div>

                <a href="{{ URL::route('room-detail') }}"><button class="see-more">SEE MORE</button></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="trusted-partners">
    <div class="container">
      <div class="headline fadeVisible">
        <span class="bold">TRUSTED</span> PARTNERS
      </div>

      <div class="partner-list">
        @for ($i=0; $i<6; $i++)
        <div class="partner-item col-xs-6 col-sm-4 fadeVisible">

        </div>
        @endfor
      </div>
    </div>
  </div>
@endsection

@section('script')
<script>
$(function(){
  $('#banner').parallax({imageSrc: '{{ URL::asset("images/banner-top.png") }}'});
  $('#special-offers').parallax({imageSrc: '{{ URL::asset("images/home-special-offer.png") }}'});
});
</script>
@endsection
