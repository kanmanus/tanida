@extends('frontend.layout.main-layout')

@section('title', ' - 2 Bedrooms')

@section('css')
  {!! Html::style('css/frontend/room-detail.css') !!}
@endsection

@section('content')
  <div id="banner">
    <div class="container">
      <div id="promo-text" class="fadeVisible">
        <div class="main">2 BEDROOMS</div>
        <div class="sub">Brightly Master bedroom with king size teakwood bed and black out curtain.</div>
      </div>
    </div>
  </div>

  <div id="room-detail">
    <div class="container">
      <div class="row">
        <div id="room-info">
          <div class="box">
            <div class="room-detail fadeVisible">
              <div class="headline">ROOM DETAIL</div>
              <div class="hr">
                <img src="{{ URL::asset('images/hr3.png') }}">
              </div>
              <div class="detail">
                <strong>Area: </strong>150 m<sup>2</sup> - 170 m<sup>2</sup><br>
                2 Bedrooms<br>
                1 Kitchen
              </div>
            </div>

            <div class="room-facilities fadeVisible">
              <div class="headline">FACILITIES</div>
              <div class="hr">
                <img src="{{ URL::asset('images/hr3.png') }}">
              </div>
              <div class="detail">
                @for ($i=0; $i<6; $i++)
                <div class="facility-item">
                  <div class="facility-icon">
                    <img src="{{ URL::asset('images/parking-icon.png') }}">
                  </div>

                  <div class="facility-name">
                    PARKING
                  </div>
                </div>
                @endfor
              </div>
            </div>
          </div>
        </div>

        <div id="room-booking">
          <div class="box">
            <div class="headline fadeVisible">
              <div class="sub">STARTING PRICE FROM</div>
              <div class="main"><span class="price">55,000</span> THB / month</div>
            </div>

            <div class="hr fadeVisible"></div>

            <form action="{{ URL::route('book-a-room') }}" method="POST" class="fadeVisible">
              <div class="form-item">
                <label for="check_in_date">CHECK IN DATE</label>
                <input type="text" id="check_in_date" name="check_in_date" placeholder="Select your date">
              </div>

              <div class="form-item">
                <label for="check_out_date">CHECK OUT DATE</label>
                <input type="text" id="check_out_date" name="check_out_date" placeholder="Select your date">
              </div>

              <div class="form-item">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit">BOOK NOW</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div id="room-gallery" class="fadeVisible">
        <img src="{{ URL::asset('images/room-image.png') }}">
      </div>

      <div id="more-rooms" class="fadeVisible">
        <div class="headline">MORE ROOMS</div>
        <div class="room-list row">
          <div class="room-item col-sm-4">
            <div class="room-image">
              <img src="{{ URL::asset('images/room-penthouse.png') }}" alt="Penthouse">
            </div>

            <div class="room-info">
              <div class="left-div">
                <div class="room-name">PENTHOUSE</div>
                <div class="room-rate">
                  30,000 THB / month
                </div>
              </div>

              <div class="right-div">
                <a href="{{ URL::route('room-detail') }}"><button class="see-more">ROOM DETAIL</button></a>
              </div>
            </div>
          </div>

          <div class="room-item col-sm-4">
            <div class="room-image">
              <img src="{{ URL::asset('images/room-1-bedroom.png') }}" alt="1 Bedroom">
            </div>

            <div class="room-info">
              <div class="left-div">
                <div class="room-name">1 Bedroom</div>
                <div class="room-rate">
                  30,000 THB / month
                </div>
              </div>

              <div class="right-div">
                <a href="{{ URL::route('room-detail') }}"><button class="see-more">ROOM DETAIL</button></a>
              </div>
            </div>
          </div>

          <div class="room-item col-sm-4">
            <div class="room-image">
              <img src="{{ URL::asset('images/room-penthouse.png') }}" alt="Penthouse">
            </div>

            <div class="room-info">
              <div class="left-div">
                <div class="room-name">PENTHOUSE</div>
                <div class="room-rate">
                  30,000 THB / month
                </div>
              </div>

              <div class="right-div">
                <a href="{{ URL::route('room-detail') }}"><button class="see-more">ROOM DETAIL</button></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script>
$(function(){
  $('#banner').parallax({imageSrc: '{{ URL::asset("images/room-detail-banner.png") }}'});
});
</script>
@endsection
