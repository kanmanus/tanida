@extends('frontend.layout.main-layout')

@section('title', ' - Book A Room')

@section('css')
  {!! Html::style('css/frontend/book-a-room.css') !!}
@endsection

@section('content')
  <div id="banner">
    <div id="promo-text" class="fadeVisible">BOOKING</div>
  </div>

  <div id="booking">
    <div class="container">
      <div id="step-bar">
        <div class="step active">
          <span class="step-no">1</span>
          <span class="step-topic">BOOK A ROOM</span>
        </div>

        <div class="step">
          <span class="step-no">2</span>
          <span class="step-topic">GUEST REVIEW</span>
        </div>

        <div class="step">
          <span class="step-no">3</span>
          <span class="step-topic">PAYMENT & CONFIRMATION</span>
        </div>
      </div>

      <div class="content row">
        <div class="col-sm-4">
          <div id="pick-your-date" class="box">
            <div class="headline">PICK YOUR DATE</div>

            <div class="form">
              <div class="form-item">
                <label for="check-in">CHECK IN</label>
                <input type="text" name="check_in" id="check-in" placeholder="Select your date">
              </div>

              <div class="form-item">
                <label for="check-out">CHECK OUT</label>
                <input type="text" name="check_out" id="check-out" placeholder="Select your date">
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-8">
          <div id="choose-a-room" class="box">
            <div class="headline">CHOOSE A ROOM</div>

            <div class="room-list">
              <div class="col-md-6">
                <div class="room-item active">
                  <div class="room-selected"><i class="glyphicon glyphicon-ok"></i> SELECTED</div>

                  <div class="row">
                    <div class="room-image col-sm-5 col-md-4">
                      <img src="{{ URL::asset('images/room-penthouse.png') }}" alt="Penthouse">
                    </div>

                    <div class="room-info col-sm-7 col-md-8">
                      <div class="room-name">PENTHOUSE</div>
                      <div class="room-detail">
                        Over, dominion own it above gathering their, don't won't waters bring male bearing form
                        may rule doesn't him fish appear spirit let earth may life you'll to great Tree moveth.
                      </div>
                      <div class="room-price">
                        <div class="topic">ROOM RATE</div>
                        <span class="strong">55,000</span> THB/month
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="room-item">
                  <div class="room-selected"><i class="glyphicon glyphicon-ok"></i> SELECTED</div>

                  <div class="row">
                    <div class="room-image col-sm-5 col-md-4">
                      <img src="{{ URL::asset('images/room-1-bedroom.png') }}" alt="1 Bedroom">
                    </div>

                    <div class="room-info col-sm-7 col-md-8">
                      <div class="room-name">1 BEDROOM</div>
                      <div class="room-detail">
                        Over, dominion own it above gathering their, don't won't waters bring male bearing form
                        may rule doesn't him fish appear spirit let earth may life you'll to great Tree moveth.
                      </div>
                      <div class="room-price">
                        <div class="topic">ROOM RATE</div>
                        <span class="strong">55,000</span> THB/month
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="room-item">
                  <div class="room-selected"><i class="glyphicon glyphicon-ok"></i> SELECTED</div>

                  <div class="row">
                    <div class="room-image col-sm-5 col-md-4">
                      <img src="{{ URL::asset('images/room-2-bedroom.png') }}" alt="2 Bedrooms">
                    </div>

                    <div class="room-info col-sm-7 col-md-8">
                      <div class="room-name">2 BEDROOMS</div>
                      <div class="room-detail">
                        Over, dominion own it above gathering their, don't won't waters bring male bearing form
                        may rule doesn't him fish appear spirit let earth may life you'll to great Tree moveth.
                      </div>
                      <div class="room-price">
                        <div class="topic">ROOM RATE</div>
                        <span class="strong">55,000</span> THB/month
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="room-item">
                  <div class="room-selected"><i class="glyphicon glyphicon-ok"></i> SELECTED</div>

                  <div class="row">
                    <div class="room-image col-sm-5 col-md-4">
                      <img src="{{ URL::asset('images/room-studio.png') }}" alt="Studio">
                    </div>

                    <div class="room-info col-sm-7 col-md-8">
                      <div class="room-name">STUDIO</div>
                      <div class="room-detail">
                        Over, dominion own it above gathering their, don't won't waters bring male bearing form
                        may rule doesn't him fish appear spirit let earth may life you'll to great Tree moveth.
                      </div>
                      <div class="room-price">
                        <div class="topic">ROOM RATE</div>
                        <span class="strong">55,000</span> THB/month
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div id="option-bar" class="text-right">
            <a href="{{ URL::route('guest-review') }}">
              <button>CONTINUE <i class="glyphicon glyphicon-menu-right"></i></button>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script>
$(function(){
  $('#banner').parallax({imageSrc: '{{ URL::asset("images/room-detail-banner.png") }}'});

  $('.room-item:not(.active)').on('click', function(){
    $('.room-item.active').removeClass('active');
    $(this).addClass('active');
  });
});
</script>
@endsection
