@extends('frontend.layout.main-layout')

@section('title', ' - Guest Review')

@section('css')
  {!! Html::style('css/frontend/guest-review.css') !!}
@endsection

@section('content')
  <div id="banner">
    <div id="promo-text" class="fadeVisible">BOOKING</div>
  </div>

  <div id="booking">
    <div class="container">
      <div id="step-bar">
        <div class="step">
          <span class="step-no">1</span>
          <span class="step-topic">BOOK A ROOM</span>
        </div>

        <div class="step active">
          <span class="step-no">2</span>
          <span class="step-topic">GUEST REVIEW</span>
        </div>

        <div class="step">
          <span class="step-no">3</span>
          <span class="step-topic">PAYMENT & CONFIRMATION</span>
        </div>
      </div>

      <div class="content">
        <div id="guest-details" class="box">
          <div class="headline">GUEST DETAILS</div>

          <div class="form">
            <div class="form-row">
              <?php $field = 'title'; $label = 'Title'; ?>
              <label for="{{ $field }}">{{ $label }}</label>

              <div class="row">
                <div class="col-xs-4 col-md-2 col-lg-1">
                  <label class="radio">
                    <input type="radio" name="{{ $field }}" value="mr"> Mr.
                  </label>
                </div>

                <div class="col-xs-4 col-md-2 col-lg-1">
                  <label class="radio">
                    <input type="radio" name="{{ $field }}" value="miss"> Miss
                  </label>
                </div>

                <div class="col-xs-4 col-md-2 col-lg-1">
                  <label class="radio">
                    <input type="radio" name="{{ $field }}" value="ms"> Ms.
                  </label>
                </div>
              </div>
            </div>

            <div class="form-row row">
              <?php $field = 'firstname'; $label = 'First Name'; ?>
              <div class="col-xs-6 col-sm-4">
                <label for="{{ $field }}">{{ $label }}</label>
                <input type="text" name="{{ $field }}" id="{{ $field }}">
              </div>

              <?php $field = 'lastname'; $label = 'Last Name'; ?>
              <div class="col-xs-6 col-sm-4">
                <label for="{{ $field }}">{{ $label }}</label>
                <input type="text" name="{{ $field }}" id="{{ $field }}">
              </div>
            </div>

            <div class="form-row row">
              <?php $field = 'email'; $label = 'E-mail'; ?>
              <div class="col-xs-6 col-sm-4">
                <label for="{{ $field }}">{{ $label }}</label>
                <input type="text" name="{{ $field }}" id="{{ $field }}">
              </div>

              <?php $field = 'tel'; $label = 'Tel.'; ?>
              <div class="col-xs-6 col-sm-4">
                <label for="{{ $field }}">{{ $label }}</label>
                <input type="text" name="{{ $field }}" id="{{ $field }}">
              </div>
            </div>
          </div>
        </div>

        <div id="special-requests" class="box">
          <div class="headline">SPECIAL REQUESTS</div>

          <div class="form">
            <div class="form-row row">
              <?php $field = 'smoking'; $label = 'Smoking'; ?>
              <div class="col-sm-3">
                <label>{{ $label }}</label>
                <div class="radio-item">
                  <label class="radio">
                    <input type="radio" name="{{ $field }}" value="n">
                    Non - Smoking
                  </label>
                </div>

                <div class="radio-item">
                  <label class="radio">
                    <input type="radio" name="{{ $field }}" value="y">
                    Smoking
                  </label>
                </div>
              </div>

              <?php $field = 'floor'; $label = 'Floor'; ?>
              <div class="col-sm-3">
                <label>{{ $label }}</label>
                <div class="radio-item">
                  <label class="radio">
                    <input type="radio" name="{{ $field }}" value="l">
                    Low Floor
                  </label>
                </div>

                <div class="radio-item">
                  <label class="radio">
                    <input type="radio" name="{{ $field }}" value="h">
                    High Floor
                  </label>
                </div>
              </div>

              <?php $field = 'bed_size'; $label = 'Bed Size'; ?>
              <div class="col-sm-6">
                <label>{{ $label }}</label>
                <div class="row">
                  <div class="radio-item col-sm-4">
                    <label class="radio">
                      <input type="radio" name="{{ $field }}" value="k">
                      King Bed
                    </label>
                  </div>

                  <div class="radio-item col-sm-4">
                    <label class="radio">
                      <input type="radio" name="{{ $field }}" value="t">
                      Twin Bed
                    </label>
                  </div>

                  <div class="radio-item col-sm-4">
                    <label class="radio">
                      <input type="radio" name="{{ $field }}" value="d">
                      Double Bed
                    </label>
                  </div>
                </div>
              </div>
            </div>

            <div class="form-row">
              <label for="other_requirement">Other Requirement(s)</label>

              <textarea name="other_requirement" id="other_requirement" rows="10"></textarea>
            </div>
          </div>
        </div>

        <div id="arrival-information" class="box">
          <div class="headline">ARRIVAL INFORMATION</div>

          <div class="form">
            <?php $field = 'flight_no'; $label = 'Flight No. / Transportation Detail'; ?>
            <div class="form-row">
              <label for="{{ $field }}">{{ $label }}</label>
              <input type="text" name="{{ $field }}" id="{{ $field }}">
            </div>

            <div class="form-row row">
              <?php $field = 'date_arrival'; $label = 'Date Arrival'; ?>
              <div class="col-xs-6">
                <label for="{{ $field }}">{{ $label }}</label>
                <input type="text" name="{{ $field }}" id="{{ $field }}">
              </div>

              <?php $field = 'expected_time'; $label = 'Expected Time of Arrival'; ?>
              <div class="col-xs-6">
                <label for="{{ $field }}">{{ $label }}</label>
                <input type="text" name="{{ $field }}" id="{{ $field }}">
              </div>
            </div>

            <div class="form-row">
              <?php $field = 'other_flight_details'; $label = 'Other Flight Details / Transportation Detail'; ?>
              <label for="{{ $field }}">{{ $label }}</label>

              <textarea name="{{ $field }}" id="{{ $field }}" rows="10"></textarea>
            </div>
          </div>
        </div>

        <div class="option">
          <a href="{{ URL::route('booking-confirmation') }}">
            <button>CONTINUE <i class="glyphicon glyphicon-menu-right"></i></button>
          </a>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script>
$(function(){
  $('#banner').parallax({imageSrc: '{{ URL::asset("images/room-detail-banner.png") }}'});
});
</script>
@endsection
