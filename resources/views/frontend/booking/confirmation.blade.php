@extends('frontend.layout.main-layout')

@section('title', ' - Payment & Confirmation')

@section('css')
  {!! Html::style('css/frontend/confirmation.css') !!}
@endsection

@section('content')
  <div id="banner">
    <div id="promo-text" class="fadeVisible">BOOKING</div>
  </div>

  <div id="booking">
    <div class="container">
      <div id="step-bar">
        <div class="step">
          <span class="step-no">1</span>
          <span class="step-topic">BOOK A ROOM</span>
        </div>

        <div class="step">
          <span class="step-no">2</span>
          <span class="step-topic">GUEST REVIEW</span>
        </div>

        <div class="step active">
          <span class="step-no">3</span>
          <span class="step-topic">PAYMENT & CONFIRMATION</span>
        </div>
      </div>

      <div class="content">
        <div id="summary" class="box">
          <div class="headline">BOOKING SUMMARY</div>

          <div class="room-detail">
            <div class="room-image">
              <img src="{{ URL::asset('images/room-penthouse.png') }}" alt="Penthouse">
            </div>

            <div class="room-info row">
              <div class="room-name col-xs-6 col-sm-3">
                <div class="topic">ROOM TYPE</div>
                <div class="value">PENTHOUSE</div>
              </div>

              <div class="room-rate col-xs-6 col-sm-3">
                <div class="topic">ROOM RATE</div>
                <div class="value">130,000 THB/month</div>
              </div>

              <div class="duration col-xs-6 col-sm-4">
                <div class="topic">DURATION</div>
                <div class="value">
                  <div class="main">12 JUL 2016 - 12 SEP 2016</div>
                  <div class="sub">(2 months)</div>
                </div>
              </div>

              <div class="total col-xs-6 col-sm-2">
                <div class="topic">TOTAL</div>
                <div class="value">260,000 THB</div>
              </div>
            </div>
          </div>
        </div>

        <div id="payment-details" class="box">
          <div class="headline">PAYMENT DETAILS</div>

          <div class="form col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
            <div class="instruction">
              Your credit card in formation is required for guarantee your reserveation only (not for payment).
              Total booking amount will be paid by cash or credit card upon check-in at the hotel.
            </div>

            <?php $field = 'payment'; ?>
            <div class="row text-center">
              <div class="col-xs-6">
                <label class="radio">
                  <input type="radio" name="{{ $field }}" value="visa"> VISA
                </label>
              </div>

              <div class="col-xs-6">
                <label class="radio">
                  <input type="radio" name="{{ $field }}" value="paypal"> PAYPAL
                </label>
              </div>
            </div>

            <div id="agreement" class="text-center">
              <label>
                <input type="checkbox" name="agreement" value="y">
                I have read and agreed to the Terms and Conditions/Cancellation policy<br><br>
              </label>

              <button type="submit" disabled="disabled" class="disabled">CONFIRM YOUR BOOKING</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script>
$(function(){
  $('#banner').parallax({imageSrc: '{{ URL::asset("images/room-detail-banner.png") }}'});

  $('input[name=agreement]').on('click', function(){
    if ($(this).is(':checked'))
      $('#agreement > button').removeClass('disabled').removeAttr('disabled');
    else
      $('#agreement > button').addClass('disabled').attr('disabled', 'disabled');
  });
});
</script>
@endsection
