$(function(){
  $(window).scroll(function() {
      if ($(document).scrollTop() > 10) {
          $('header').addClass('mini');
      }
      else {
          $('header').removeClass('mini');
      }
  });
  
  $('.fadeVisible').addClass("viewport-hidden").viewportChecker({
    classToAdd: 'viewport-visible animated fadeIn',
    offset: 100
  });

  $('.nav-toggle-icon').on('click', function(){
    $('.nav-xs').toggle();
  });
});
